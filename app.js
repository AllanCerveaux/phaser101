const config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	scene: {
		preload: preload,
		create: create,
		update: update
	}
}

const game = new Phaser.Game(config);

function preload(){
	this.load.image('logo', './assets/logo.png');
	this.load.spritesheet('player', './assets/lizard_m_idle.png', {
		frameWidth: 16,
		frameHeight: 26
	});
	this.load.spritesheet('player-move', './assets/lizard_m_run.png', {
		frameWidth: 16,
		frameHeight: 28
	});
}

function create(){
	const logo = this.add.image(400, 150, 'logo');
	this.tweens.add({
		targets: logo,
		y: 450,
		duration: 2000,
		ease: "Power2",
		yoyo: true,
		loop: -1
	});
	player = this.add.sprite(100, 100, 'player');
	player.setDepth(11)
	this.anims.create({
		key: 'player-idle',
		frames: this.anims.generateFrameNumbers('player', {start: 0, end:4}),
		frameRate: 8,
		repeat: -1
	})
	player.play('player-idle');
	this.anims.create({
		key: 'player-run',
		frames: this.anims.generateFrameNumbers('player-move', {start: 0, end:4}),
		frameRate: 4,
		repeat: -1
	})
	// player.play('player-run');
	player.setScale(2);
	keys = this.input.keyboard.createCursorKeys();
	moving = false;
}

function update(t, dt){
	if(keys.left.isDown) {
		player.setFlip(true);
	}
	else if(keys.right.isDown) {
		player.setFlip(false);
	}
	if(keys.left.isDown || keys.right.isDown) {
		moving = true;
		player.play('player-run', true);
	} 
	else if (moving) {
		moving = false;
		player.play('player-idle', true);
	}
}